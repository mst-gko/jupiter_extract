#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import datetime
import os

import psycopg2 as pg
import pandas as pd
from openpyxl import load_workbook
import shapefile  # pip install pyshp
import configparser
import pygeoif

__title__ = 'Jupiter Extract'
__authors__ = 'Simon Makwarth'
__version__ = "1.2.0"
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth"
__email__ = "simak@mst.dk"
__status__ = "Production"
__repository__ = f"https://gitlab.com/mst-gko/{__title__.lower().replace(' ', '_')}"
__issues__ = f'{__repository__}/-/issues'
__description__ = f"""
                    {__title__} extracts chemical data from local Jupiter database tables 
                    and creates an MS spreadsheet containing the data.
                    """
__requirements__ = f"""
                    {__title__} requires access to the MST-GKO common drive. 
                    The ini-file F:/GKO/data/grukos/db_credentials/reader/reader.ini is used
                    to fetch database credentials.
                    """


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        ini_section = self.ini_database_name
        user = config[f'{ini_section}']['userid']
        password = config[f'{ini_section}']['password']
        host = config[f'{ini_section}']['host']
        port = config[f'{ini_section}']['port']
        database = config[f'{ini_section}']['databasename']

        return user, password, host, port, database

    def connect_to_pg_db(self):
        user, password, host, port, database = self.parse_db_credentials()
        try:
            pg_con = pg.connect(
                host=host,
                port=port,
                database=database,
                user=user,
                password=password
            )
            print('Connected to database: ' + database)
            return pg_con, database
        except Exception as e:
            print('Unable to connect to database: ' + database)
            print(e)

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query for the import
        :return: pandas dataframe containing output from sql query
        """

        conn, database = self.connect_to_pg_db()
        try:
            print(f'Fetching table from {database} to dataframe...')
            df = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            print(f'Table loaded into dataframe from database: {database}')
            return df


class Shapefile:
    def __init__(self, shp_path):
        self.shp_path = shp_path

    def cte_geom_from_shp(self):
        sf = shapefile.Reader(self.shp_path)
        wkt_shp_geom = ''

        for k, s in enumerate(sf.shapes()):
            if sf.shapeType in [5, 15, 25]:
                poly = pygeoif.geometry.as_shape(s)
            else:
                raise ValueError('Shapefile geometry is not Polygon.')

            if k == 0:
                wkt_shp_geom += f"""
                                SELECT 
                                    ST_GeomFromText('{poly}', 25832) as geom"""
            else:
                wkt_shp_geom += f""" 
                                UNION ALL 
                                SELECT 
                                    ST_GeomFromText('{poly}', 25832) as geom"""

        return wkt_shp_geom


class ChemExtract:
    def __init__(self, shp_path):
        self.shp_path = shp_path

    @staticmethod
    def add_to_excel(path, df_input, sheet_name):
        """function to add aditional sheets to excelfile"""
        writer_add = pd.ExcelWriter(path, engine='openpyxl')
        book = load_workbook(path)
        writer_add.book = book
        df_input.to_excel(writer_add, sheet_name=sheet_name, index=False)
        writer_add.save()
        writer_add.close()
        print(f'### Worksheet done: {sheet_name} ###')

    @staticmethod
    def create_excel(path, df_input, sheet_name):
        """ function to delete existing spreadsheet and creating new spreadsheet containing the initial sheet """
        if os.path.exists(path):
            os.remove(path)
            print(f'### Deleting: {path} ###')
        else:
            print(f'### {path} does not exist, skipping deletion ###')
        print(f'### Creating: {path} ###')
        df_input.to_excel(path, sheet_name=sheet_name, index=False)
        print(f'### Worksheet done: {sheet_name} ###')

    def chem_extract(self):
        filename = os.path.splitext(os.path.basename(self.shp_path))[0]
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d')
        path_output = f'./Kemiudtraek_{filename}_{st}.xlsx'

        # check if output file is in use by another process
        if os.path.exists(path_output):
            try:
                os.rename(path_output, path_output)
            except OSError as e:
                print(e)
                exit()

        # create sql from within shapefile geometry
        sf = Shapefile(self.shp_path)
        cte_shp_geom = sf.cte_geom_from_shp()
        # define sheetname and sql in list of tuples
        sheetnames_sqls = [
            (
                "Alt Data",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT 
                        bh.* 
                    FROM mstjupiter.mstmvw_substance_all_data bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Arbejdsdata",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT 
                        bh.* 
                    FROM mstjupiter.mstmvw_substance_all_data bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Database",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT 
                        qac.* 
                    FROM mstjupiter.quality_assurance_chemistry qac
                    INNER JOIN tmp ON st_within(qac.geom, tmp.geom)
                '''
            ),
            (
                "Magasin",
                f'''
                    WITH tmp AS ({cte_shp_geom})
                        SELECT 
                            bh.boreholeno, 
                            sc.intakeno,	
                            bh.xutm32euref89, 
                            bh.yutm32euref89, 
                            sc.top, 
                            sc.bottom, 
                            NULL AS magasin
                        FROM jupiter.borehole bh 
                        INNER JOIN jupiter.screen sc using (boreholeno)  
                        ORDER BY bh.boreholeno, sc.intakeno
                '''
            ),
            (
                "Nitrat",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_nitrate_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Sulfat-nitrat",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Sulfat",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_sulphate_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Klorid",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_chloride_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Klorid-sulfat",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_chloride_and_sulphate_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Ionbytning",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_sodium_chloride_ionexchange_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "NVOC",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_nvoc_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Arsen",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_arsenic_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Vandtype",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_watertype_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Jern",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_iron_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            ),
            (
                "Strontium",
                f'''
                    WITH tmp AS ({cte_shp_geom}) 
                    SELECT bh.* 
                    FROM mstjupiter.mstmvw_substance_strontium_latest bh
                    INNER JOIN tmp ON st_within(bh.geom, tmp.geom)
                '''
            )
        ]

        list_colums_drop = [
            'geom',
            'utmzone',
            'guid_intake',
            'row_id'
        ]

        db = Database(database_name='jupiter', usr='reader')
        for k, row in enumerate(sheetnames_sqls):
            sheet_name = row[0]
            sql = row[1]
            print(sql)
            df = db.import_pgsql_to_df(sql)
            for column in df.columns:

                if column in list_colums_drop:
                    # df.drop(columns=[f'{column}'])
                    df.pop(column)

            if k == 0:
                self.create_excel(path_output, df, sheet_name)
            else:
                self.add_to_excel(path_output, df, sheet_name)

        print('### Excel file created ###')


# start timer
t = time.time()

# # EDIT SHAPE PATH HERE # #
shp_path = r'F:\GKO\drift\Rødding-Tiset\03_GIS\01_Data\Modelomr\oplaeg_RDK_no17nov2021\Modelområde_udvidet_EPSG25832.shp'

ce = ChemExtract(shp_path=shp_path)
ce.chem_extract()

# end timer and print time spend for execution
elapsed = round(time.time() - t, 2)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')