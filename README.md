# Jupiter extract
This application is an interactive tool for extracting chemical samples 
from the Jupiter database. For the program to launch successfully,
it must be launched from the MST-GKO domain.

## Output (Danish)
I dette afsnit beskrives de enkelte faneblade i excel dokumentet.
-	Alt Data: Dette faneblad indeholder en liste med alle kemi analyser, oprette som en pivot tabel, men en række per sampleid.
-	Arbejdsdata: Dette faneblad er en delmængde af fanebladet ”Alt Data”, her frasorteres kemi prøver der:
--	tilhøre boringer ikke har en geometri
--	ikke er tilknyttet et indtag 
--	udelukkende indeholder analyser af 'temperatur', 'tørstof_total', 'ph' og/eller 'konduktivitet'
-	Arsen: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder arsen. 
-	Klorid-sulfat: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder både klorid og sulfat. 
-	Klorid: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder klorid. 
-	Jern: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder jern. 
-	NVOC: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder ’Carbon,org,NVOC’. 
-	Vandtype: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, hvor der kan beregnes vandtype. Altså skal prøven som minimum indeholder alle tre analyser af nitrat, jern og sulfat.
-	Sulfat-nitrat: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder både nitrat og sulfat. 
-	Nitrat: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder nitrat.
-	Ionbytning: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, hvor der kan beregnes ionbytning. Altså skal prøven som minimum indeholder analyser af både natrium og klorid.
-	Sulfat: Dette faneblad er en delmængde af fanebladet ”Arbejdsdata”, der indeholder den seneste kemi prøve for et indtag, der indeholder sulfat.

## Installation
Extract the zip file from the repository, and execute or double-click 
the `Jupiter_extract.exe`. No administrative credentials are required 
to run the program.

Zipfile can be downloaded from here:
- [Windows download link](https://gitlab.com/s.makwarth/kemi_data/blob/master/Jupiter_extract.zip)

**NOTE**: Since Jupiter extract is a new program, you may encounter anti-virus 
warnings on the first launch.

## Dependencies
The git repository pgjupiter is needed to run this problam:
- https://gitlab.com/mst-gko/pgjupiter

To keep the MST-GKO's local pcjupiterxl datadase updated all sql files restore_*.sql is needed (e.g. restore_watlevel.sql)

To generate the derived tables used for this program, the sql file Substance_chemsamp_merged.sql is needed.

## Help
Please see the included documentation, available from the "Help" menu within 
the application, or from the [help](help/help.html) directory.

## Author and License
Jupiter extract is written by [Simon Makwarth](mailto:simak@mst.dk), and is 
licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.
