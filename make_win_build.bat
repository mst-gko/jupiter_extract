@echo off
REM Run this script on a 64-bit Windows machine in a Anaconda Python 3 prompt

cd C:\gitlab\jupiter_extract

echo Remove previous build directory
if exist "Jupiter_extract\" rmdir /S /Q Jupiter_extract
if exist "build\" rmdir /S /Q build
if exist "dist\" rmdir /S /Q dist
if exist "Jupiter_extract.zip" del "*.zip" /s /f /q
if not exist "Jupiter_extract\" mkdir "Jupiter_extract"

echo Build stand-alone application
pyinstaller --onefile --windowed --icon icon.ico Jupiter_extract.py

echo Copy auxillary files to build directory
xcopy /S /E help Jupiter_extract\help\
copy icon.ico Jupiter_extract\
copy LICENSE Jupiter_extract\
copy README.md Jupiter_extract\
copy dist\Jupiter_extract.exe Jupiter_extract\
echo Build complete, output located in dist/

echo zipping folder
SET SourceDir=C:\folder\source
SET DestDir=C:\folder\destination
SET sevenzip_path="C:\Program Files\7-Zip\7z.exe"
%sevenzip_path% a -tzip "C:\gitlab\jupiter_extract\Jupiter_extract.zip" "C:\gitlab\jupiter_extract\Jupiter_extract\"

rmdir /S /Q dist
rmdir /S /Q build
rmdir /S /Q __pycache__
del "*.spec" /s /f /q
rmdir /S /Q Jupiter_extract
