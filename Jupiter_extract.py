#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import *
from tkinter import filedialog as fd
import time
import datetime
import subprocess
import os
import shutil

import psycopg2 as pg  # pip install psycopg2
import pandas as pd  # pip install pandas
import shapefile  # pip install pyshp
import configparser  # conda install configparser
import pygeoif
# import xlsxwriter
# from openpyxl import load_workbook  # pip install openpyxl
import xlwings as xw

__title__ = 'Jupiter Extract'
__authors__ = 'Simon Makwarth <simak@mst.dk>'
__version__ = "2.0.0"
__license__ = "GPLv3"
__maintainer__ = "Simon Makwarth <simak@mst.dk>"
__status__ = "Production"
__repository__ = f"https://gitlab.com/mst-gko/{__title__.lower().replace(' ', '_')}"
__issues__ = f'{__repository__}/-/issues'
__description__ = __title__ + ''' extracts chemical data from local Jupiter database tables
and creates an MS spreadsheet containing the data.'''
__dependensies__ = 'https://gitlab.com/mst-gko/pgjupiter/Substance_chemsamp_merged.sql'


class Shapefile:
    def __init__(self, shp_path):
        self.shp_path = shp_path
        self.sf = shapefile.Reader(self.shp_path)

    def fetch_cte_from_shp_geom(self):
        cte_shp_geom = ''
        for k, s in enumerate(self.sf.shapes()):
            poly = pygeoif.geometry.as_shape(s)
            if k == 0:
                cte_shp_geom += f"""
                                    SELECT 
                                    ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            elif k > 0:
                cte_shp_geom += f""" 
                                    UNION ALL 
                                    SELECT
                                        ST_MakeValid(ST_GeomFromText('{poly}', 25832)) as geom"""
            else:
                pass

        return cte_shp_geom


class Database:
    def __init__(self, database_name='JUPITER', usr='reader'):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials()

        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            del cred
            # print(f'Connected to database: {pg_database_name}')

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql


class Xlsx:
    def __init__(self, xlsx_path, template_xlsx='./dependencies/template_udtraek_kemidatabase.xlsm'):
        self.xlsx_path = xlsx_path
        self.template_xlsx = template_xlsx

    def copy_template_xlsx(self):
        if os.path.exists(self.xlsx_path):
            os.remove(self.xlsx_path)
        shutil.copyfile(self.template_xlsx, self.xlsx_path)

    def append_df_to_xlsm(self, df, sheetname, startfromcell, protect=0):
        with xw.App() as app:
            wb = xw.Book(self.xlsx_path)
            try:
                wb.sheets.add(sheetname, after='Arbejdsdata')
            except ValueError as V:
                pass
            ws = wb.sheets[sheetname]

            if protect == 1:
                ws.api.Unprotect()

            ws[f"{startfromcell}"].options(pd.DataFrame, header=1, index=False, expand='table').value = df
            ws[f"{startfromcell}"].expand("right").api.Font.Bold = True
            ws[f"{startfromcell}"].expand("right").api.Borders.Weight = 2

            if protect == 1:
                ws.api.Protect()

            wb.save(self.xlsx_path)
            wb.close()


class Menu(tk.Frame):
    def __init__(self, master, idle_input, *args, **kwargs):
        tk.Frame.__init__(self, master, *args, **kwargs)
        self.master = master
        self.idle_input = idle_input
        self.init_menu()

    def get_shape_dir(self):
        """Button for selecting the shapefile containing the area of interest"""
        shape_dir = fd.askopenfilename(initialdir='c:/', title='Please select a ESRI shape file',
                                       filetypes=(('ESRI Shape file', '*.shp'), ("All files", "*.*")))
        sf = shapefile.Reader(shape_dir)
        # check if shape dir exist and if shapefile is polygon (shapetype 5) with only a single row
        if shape_dir:
            if sf.shapeType not in [1, 11, 21, 5, 15, 25]:
                self.statusbar['text'] = 'Shapefile must be a point or polygon not line/multipoint'
            else:
                self.var_shape.set(shape_dir)
                self.label_shape['textvariable'] = self.var_shape
                self.label_shape['fg'] = 'black'
                self.statusbar['text'] = 'Shapefile loaded'

    def get_output_dir(self):
        """Button for selecting the output directory for the MS spreadsheet"""
        output_dir = fd.askdirectory(initialdir='c:/', title='Please select a directory')
        if output_dir:
            self.var_dir.set(output_dir)
            self.label_dir['textvariable'] = self.var_dir
            self.label_dir['fg'] = 'black'
            self.statusbar['text'] = 'Output directory loaded'

    def on_press(self, ):
        """Button for changing the statusbar when pressing the button"""
        var_output = self.var_dir.get()
        path_shape = self.var_shape.get()
        if var_output != self.idle_input[0] and path_shape != self.idle_input[1]:
            self.statusbar['text'] = 'Program initiated...0 %'
        else:
            self.statusbar['text'] = 'Insert path to shapefile and output before running'

    def run_script(self):
        """Button for running fetching the db view and making an MS spreadsheet
            at the selected output directory within the selected shapefile"""
        var_output = self.var_dir.get()
        path_shape = self.var_shape.get()
        idle_output_path = self.idle_input[0]
        idle_shape_path = self.idle_input[1]

        filename = os.path.splitext(os.path.basename(path_shape))[0]
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d')
        path_output = var_output + '/Kemiudtraek_' + filename + '_' + st + '.xlsm'

        # check if output file is in use by another process
        var_is_in_use = 0
        if os.path.exists(path_output):
            try:
                os.rename(path_output, path_output)
            except OSError as e:
                print(e)
                var_is_in_use = 1

        if var_output != idle_output_path and path_shape != idle_shape_path and var_is_in_use != 1:
            shp = Shapefile(path_shape)
            cte_string = shp.fetch_cte_from_shp_geom()

            table_infos = [
                (1, 'Alle Data', 'mstmvw_substance_all_data', 'A1', 0),
                (1, 'Arbejdsdata', 'mstmvw_substance_all_data_filtered', 'A1', 0),
                (self.var_magasin.get(), 'Magasin', 'mstmvw_substance_aquifer', 'A1', 0),
                (self.var_nitrat.get(), 'Nitrat seneste', 'mstmvw_substance_nitrate_latest', 'A1', 0),
                (self.var_sulfat.get(), 'Sulfat seneste', 'mstmvw_substance_sulphate_latest', 'A1', 0),
                (self.var_sul_nit.get(), 'Sulfat-nitrat seneste', 'mstmvw_substance_nitrate_and_sulphate_latest', 'A1', 0),
                (self.var_klorid.get(), 'Klorid seneste', 'mstmvw_substance_chloride_latest', 'A1', 0),
                (self.var_klo_sul.get(), 'Klorid-sulfat seneste', 'mstmvw_substance_chloride_and_sulphate_latest', 'A1', 0),
                (self.var_ionbyt.get(), 'Ionbytning seneste', 'mstmvw_substance_sodium_chloride_ionexchange_latest', 'A1', 0),
                (self.var_nvoc.get(), 'NVOC seneste', 'mstmvw_substance_nvoc_latest', 'A1', 0),
                (self.var_arsen.get(), 'Arsen seneste', 'mstmvw_substance_arsenic_latest', 'A1', 0),
                (self.var_vandtyp.get(), 'Vandtype seneste', 'mstmvw_substance_watertype_latest', 'A1', 0),
                (self.var_jern.get(), 'Jern seneste', 'mstmvw_substance_iron_latest', 'A1', 0),
                (self.var_strontium.get(), 'Strontium seneste', 'mstmvw_substance_strontium_latest', 'A1', 0),
                (1, 'Database', 'mstmvw_substance_database', 'E1', 1)
            ]
            db = Database()
            xlsx = Xlsx(path_output)
            xlsx.copy_template_xlsx()
            for i, table_info in enumerate(reversed(table_infos)):
                incl_sheet = table_info[0]  # 1 if sheet should be included in xlsx, 0 if not
                sheet_name = table_info[1]
                table_name = table_info[2]
                startfromcell = table_info[3]
                protect_sheet = table_info[4]

                if incl_sheet == 1:
                    sql = f'''
                            WITH geo AS ({cte_string})
                            SELECT t.* 
                            FROM mstjupiter.{table_name} t
                            INNER JOIN geo g ON ST_INTERSECTS(t.geom, g.geom)
                    '''
                    df = db.import_pgsql_to_df(sql=sql)

                    remove_columns = [
                        'geom', 'row_id', 'lith_in_screen', 'fohm_layer', 'guid_intake'
                    ]
                    for column_name in remove_columns:
                        if column_name in df:
                            df.pop(column_name)

                    xlsx.append_df_to_xlsm(df=df, sheetname=sheet_name, startfromcell=startfromcell, protect=protect_sheet)
                    self.statusbar['text'] = f'Program initiated. ' \
                                             f'Loading layers ({round(i / len(table_infos) * 100, 0)} %)'
                    self.master.update_idletasks()
            self.statusbar['text'] = f'Program initiated. Saving Spreadsheet... %'
            self.master.update_idletasks()

            self.statusbar['text'] = 'Program finished. Spreadsheet completed'
            self.master.update_idletasks()
        elif var_output != idle_output_path and path_shape != idle_shape_path and var_is_in_use != 0:
            self.statusbar['text'] = 'Could not create file, seems to be open by another program'
        else:
            self.statusbar['text'] = 'Insert path to shapefile and output before running'

    def init_menu(self):
        # add title
        frame_title = Frame(self.master)
        frame_title.pack(side=TOP)
        label_title = Label(frame_title, text=f"{__title__} ver. {__version__}", font='none 14 bold')
        label_title.grid(row=0, column=0, sticky=W)

        # add status bar
        frame_statusbar = Frame(self.master)
        frame_statusbar.pack(side=BOTTOM)
        self.statusbar = Label(
            frame_statusbar,
            text='Ready to receive input paths',
            bd=3,
            relief=SUNKEN,
            font='Helvetica 9 bold'
        )
        self.statusbar.pack()
        self.statusbar.config(width="60", anchor="w")

        # insert frame for user inputs
        frame_input = Frame(self.master)
        frame_input.pack()

        # request shape file - area of interest
        self.var_shape = StringVar()  # init variable for shapefile directory
        title_shape = Label(frame_input, text="Insert shape-file directory:", font='none 10 bold')
        title_shape.grid(row=2, column=0, sticky=W)
        self.var_shape.set(self.idle_input[1])
        self.label_shape = Label(frame_input, textvariable=self.var_shape, bg='white', fg='grey', borderwidth=2,
                                 relief="groove", anchor="w")
        self.label_shape.config(height=1, width=50)
        self.label_shape.grid(row=3, column=0, sticky=W)
        button_shape = Button(frame_input, text='...', width=3, command=self.get_shape_dir)
        button_shape.grid(row=3, column=1)

        # request output directory
        self.var_dir = StringVar()  # init variable for output directory
        title_dir = Label(frame_input, text="Insert output directory:", font='none 10 bold')
        title_dir.grid(row=4, column=0, sticky=W)
        self.var_dir.set(self.idle_input[0])
        self.label_dir = Label(frame_input, textvariable=self.var_dir, bg='white', fg='grey', borderwidth=2,
                               relief="groove", anchor="w")
        self.label_dir.config(height=1, width=50)
        self.label_dir.grid(row=5, column=0, sticky=W)
        button_dir = Button(frame_input, text='...', width=3, command=self.get_output_dir)
        button_dir.grid(row=5, column=1, sticky=W)

        # request selected datasheet
        self.var_magasin = IntVar()
        self.var_nitrat = IntVar()
        self.var_sulfat = IntVar()
        self.var_sul_nit = IntVar()
        self.var_klorid = IntVar()
        self.var_klo_sul = IntVar()
        self.var_ionbyt = IntVar()
        self.var_nvoc = IntVar()
        self.var_arsen = IntVar()
        self.var_vandtyp = IntVar()
        self.var_jern = IntVar()
        self.var_strontium = IntVar()

        title_check = Label(frame_input, text="Add additional sheets:", font='none 10 bold')
        title_check.grid(row=6, column=0, sticky=W)
        chkbox_input = Frame(self.master)
        chkbox_input.pack()
        c2 = Checkbutton(chkbox_input, text="Magasin", variable=self.var_magasin, onvalue=1, offvalue=0)
        c2.grid(row=7, column=0, sticky=W)
        c3 = Checkbutton(chkbox_input, text="Nitrat, Seneste", variable=self.var_nitrat, onvalue=1, offvalue=0)
        c3.grid(row=7, column=1, sticky=W)
        c4 = Checkbutton(chkbox_input, text="Sulfat, Seneste", variable=self.var_sulfat, onvalue=1, offvalue=0)
        c4.grid(row=7, column=2, sticky=W)
        c5 = Checkbutton(chkbox_input, text="Sulfat-nitrat, Seneste", variable=self.var_sul_nit, onvalue=1, offvalue=0)
        c5.grid(row=8, column=0, sticky=W)
        c6 = Checkbutton(chkbox_input, text="Klorid, Seneste", variable=self.var_klorid, onvalue=1, offvalue=0)
        c6.grid(row=8, column=1, sticky=W)
        c7 = Checkbutton(chkbox_input, text="Klorid-Sulfat, Seneste", variable=self.var_klo_sul, onvalue=1, offvalue=0)
        c7.grid(row=8, column=2, sticky=W)
        c8 = Checkbutton(chkbox_input, text="Ionbytning, Seneste", variable=self.var_ionbyt, onvalue=1, offvalue=0)
        c8.grid(row=9, column=0, sticky=W)
        c9 = Checkbutton(chkbox_input, text="NVOC, Seneste", variable=self.var_nvoc, onvalue=1, offvalue=0)
        c9.grid(row=9, column=1, sticky=W)
        c10 = Checkbutton(chkbox_input, text="Arsen, Seneste", variable=self.var_arsen, onvalue=1, offvalue=0)
        c10.grid(row=9, column=2, sticky=W)
        c11 = Checkbutton(chkbox_input, text="Vandtype, Seneste", variable=self.var_vandtyp, onvalue=1, offvalue=0)
        c11.grid(row=10, column=0, sticky=W)
        c12 = Checkbutton(chkbox_input, text="Jern, Seneste", variable=self.var_jern, onvalue=1, offvalue=0)
        c12.grid(row=10, column=1, sticky=W)
        c13 = Checkbutton(chkbox_input, text="Strontium, Seneste", variable=self.var_strontium, onvalue=1, offvalue=0)
        c13.grid(row=10, column=2, sticky=W)

        # add buttons
        frame_buttons = Frame(self.master)
        frame_buttons.pack()
        button_run = Button(frame_buttons, text='Run', width=6, command=self.run_script)
        button_run.pack()
        # button_run.bind("<ButtonPress>", self.on_press())
        # button_run.bind("<ButtonRelease>", self.run_script())

        # add icon
        icon = os.getcwd() + '/icon.ico'
        self.master.iconbitmap(icon)


class Menubar(tk.Frame):
    """Builds a menu bar for the top of the main window"""

    def __init__(self, master, idle_input, *args, **kwargs):
        """ Constructor"""
        tk.Frame.__init__(self, master, *args, **kwargs)
        self.master = master
        self.init_menubar()
        self.idle_input = idle_input

    @staticmethod
    def open_url_in_browser(url):
        """
        Open an URL
        :param url: The path for help-html file
        """

        if os.name == 'nt':
            opencmd = 'explorer'
        elif os.name == 'posix':
            opencmd = 'xdg-open'
        elif os.name == 'mac':
            opencmd = 'open'
        else:
            opencmd = None

        if opencmd:
            subprocess.call([opencmd, url])
        else:
            pass

    def on_exit(self):
        """Exits program"""
        self.master.destroy()

    def display_help(self):
        """Initiating the help html-file"""
        self.open_url_in_browser(f'{os.getcwd()}.\\help\\help.html')

    def display_about(self):
        """Initiating git repo url containing information about program"""
        self.open_url_in_browser(__repository__)

    def display_issues(self):
        self.open_url_in_browser(__issues__)

    def init_menubar(self):
        menubar = tk.Menu(self.master)
        menu_file = tk.Menu(menubar)  # Creates a "File" menu
        menu_file.add_command(label='Exit', command=self.on_exit)  # Adds an option to the menu
        menubar.add_cascade(menu=menu_file, label='File')  # Adds File menu to the bar.

        menu_help = tk.Menu(menubar)  # Creates a "Help" menu
        menu_help.add_command(label='Help', command=self.display_help)
        menu_help.add_command(label='About', command=self.display_about)
        menu_help.add_command(label='Issues', command=self.display_issues)
        menubar.add_cascade(menu=menu_help, label='Help')

        self.master.config(menu=menubar)


class MainApplication:
    """Init the program"""

    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.master.option_add('*tearOff', False)  # removes dashline from menu
        self.frame.pack()
        self.idle_input_output = 'E.g. c:/folder'
        self.idle_input_shape = 'E.g. c:/shape_file.shp'
        self.idle_input = [self.idle_input_output, self.idle_input_shape]

        # init menu and menubar
        self.master.title(f'{__title__}')
        self.menu = Menu(self.master, self.idle_input)
        self.menubar = Menubar(self.master, self.idle_input)


def main():
    root = tk.Tk()
    MainApplication(root)
    root.mainloop()


if __name__ == '__main__':
    main()
