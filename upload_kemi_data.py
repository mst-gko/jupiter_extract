import datetime

import pandas as pd
import psycopg2 as pg
import configparser
from sqlalchemy import create_engine


class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def fetch_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def fetch_pg_engine(self):
        """
        Connects to postgresql server database
        :return: postgresql sqlalchemy connection engine and string database name
        """
        cred = self.fetch_db_credentials()

        pg_database_name = cred['dbname']
        pg_conn_str = f"postgresql+psycopg2://{cred['usr']}:{cred['pw']}@{cred['host']}:{cred['port']}/{cred['dbname']}"
        pg_engine = create_engine(pg_conn_str)
        del cred

        return pg_engine, pg_database_name

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """

        cred = self.fetch_db_credentials()
        pgdatabase = cred['dbname']

        try:
            with pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred

    def export_df_to_pg_db(self, df, schemaname, tablename, if_exists='replace'):
        """
        Export pandas df to postgresDB
        :param df: pandas dataframe
        :param schemaname: database schema to export dataframe to
        :param tablename: database table to export dataframe to
        :param if_exists: how to act if table exists in database: 'replace' (default) or 'append'
        :return: None
        """

        sql_engine, pgdatabase = self.fetch_pg_engine()
        try:
            df.columns = df.columns.str.lower()
            df.to_sql(tablename, con=sql_engine, schema=schemaname, if_exists=if_exists, index=False)
        except Exception as e:
            print('Table could not be edited: ' + pgdatabase + '.' + schemaname + '.' + tablename)
            print(e)

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        sql_engine, pgdatabase = self.fetch_pg_engine()
        conn = sql_engine.connect()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {pgdatabase}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql


# EDIT HERE
path_chemistry_xlsm = r'F:\GKO\drift\Jammerbugt\Data\grundvandskemisk kortlægning\Kemiudtraek\Jammerbugt_Kemiudtraek_v1.xlsm'
person_kemisk_kortlaegning = 'MST JEBMA'
person_upload = 'simak'

# database information
database_name_upload = 'GKO'
schema_name_upload = 'dataquality'
table_name_upload = 'qa_grwchemanalysis'

# indlæs data
df = pd.read_excel(path_chemistry_xlsm, sheet_name='Database')

# data wrangling
df = df[df['Er godkendt'].notnull()]
df['is_approved'] = df['Er godkendt'].astype(bool)
df = df.loc[:, ['guid_grwchemanalysis', 'is_approved']]
df['person'] = person_kemisk_kortlaegning
df['opretbruger'] = person_upload
df['opretdato'] = datetime.datetime.now()
df['opdateringbruger'] = person_upload
df['opdateringdato'] = datetime.datetime.now()

# eksporter data til grukos
db = Database(database_name=database_name_upload, usr='writer')
db.export_df_to_pg_db(df, schemaname=schema_name_upload, tablename=table_name_upload, if_exists='append')
